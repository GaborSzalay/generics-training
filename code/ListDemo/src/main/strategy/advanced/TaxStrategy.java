package strategy.advanced;

public interface TaxStrategy {
	long calculateTax(TaxPayer p);
}
